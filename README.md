
#Ctools Cockie Context
The CTools cookie context is a CTools context plugin,
that provide a new context that will grab a certain cookie value and
use it as a CTools context.

Install instructions:  

1.  Download module and enable it
2. go to you panel page and context will be availible in the "Add context" area.
